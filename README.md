# WeatherBot

Weather bot: [@SergeysWeatherBot](https://t.me/@SergeysWeatherBot)
The launch script is located at `main.py`. The output example is.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_6EB2B84C925C5E68969F8E917091A94D4D92F0A1B30AD0144C7FCDD698121659_1525684276728_2018-05-05_21-45-15.png)

## Used API

The weather data is retrieved from [google.com](https://www.google.com). As it
may be seen, typical weather request like "Погода в Москве завтра" yields
a weather tile.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_6EB2B84C925C5E68969F8E917091A94D4D92F0A1B30AD0144C7FCDD698121659_1525684321822_2018-05-07_12-11-49.png)


The idea is pretty straightforward: to use the search engine’s response, we need to

- Make an http request, mimicking a web browser’s behaviour and get the response page with the shown tile.
- Parse the `.html` of the response and extract the tile.
- Convert the html fragment to text.
- Extract the weather data we seek from the text.

**Implementation**

- The request is made using gorgeous Python package called `requests` . The appropriate behaviour is reached by specifying the correct User-Agent:

```python
HEADER = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) '
                        'AppleWebKit/537.36 (KHTML, like Gecko) '
                        'Chrome/67.0.3396.18 Safari/537.36'}
url = "https://www.google.ru/search?q={}&hl=ru".format(
        "+".join(request)
    )
response = requests.get(url, headers=HEADER)
```

- As I’ve found using **inspect element** feature of Google Chrome, the desired data is stored in `div` element of class `card-section`. Using another package called BeautifulSoup, we find the element in the HTML.
- The next step is to transform the element to the plain text. It’s performed by `html2text` package.
- The structure of the output is somewhat same for different requests, so the final step is to parse the text output and convert it to the `Weather` object. The class itself and the logic I’ve introduced above is located at `google.py` file.
- In order to avoid dealing with raw Telegram API, I used a package called `telepot`, that makes bot implementation pretty simple. The bot’s job is pretty simple, it forwards every request to Google adding word “погода” if it’s not present, retrieves `Weather` object and composes the response.
- The response consists of basic weather info as well as a temperature plot built by `matplotlib` and a cool photo of the location.
- The location photo is retrieved from Google Images service. In order to do that, I’ve used a package `google_images_download`. The cool thing that made everything much simpler is that the location name is stored in the `Weather` object as it can be extracted from the weather tile from google.com.
- The bot itself runs using a free instance of Amazon EC2.

## Dependencies

Python 3.6 and the latest packages of

* [`requests`](http://docs.python-requests.org/en/master/)
* [`telepot`](https://github.com/nickoala/telepot)
* [`google_images_download`](https://github.com/hardikvasa/google-images-download)
* [`BeautifulSoup`](https://www.crummy.com/software/BeautifulSoup/bs4/doc/#)
* [`html5lib`](https://html5lib.readthedocs.io/en/latest/)
* [`html2text`](https://github.com/aaronsw/html2text)
* [`matplotlib`](https://matplotlib.org/)

You can install the dependencies via pip

```bash
pip3 install requests telepot google_images_download html2text beautifulsoup4 matplotlib html5lib
```
