from google_images_download import google_images_download

def get_picture_for(request: str, folder: str="temp"):
    """"""
    response = google_images_download.googleimagesdownload()
    arguments = {"keywords": request, "limit": 1, "output_directory": folder,
                 "print_urls": True}  # creating list of arguments
    response.download(arguments)