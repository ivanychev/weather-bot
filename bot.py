import collections
import enum
import sys
import traceback
import types

import telepot
import telepot.loop

import google
import google_image
import utils

import matplotlib
# Force matplotlib to not use any Xwindows backend.
matplotlib.use('Agg')
import matplotlib.pyplot as plt

TEMP_DIRECTORY_NAME = ".tmp"
TEMP_PLOT_FILENAME = TEMP_DIRECTORY_NAME + "/pic.png"

class TemperatureDegree(enum.Enum):
    Celsius = 1
    Fahrenheit = 2


ChatMeta = collections.namedtuple("ChatMeta", ["temp_degree"])
DEFAULT_META = ChatMeta(TemperatureDegree.Celsius)


class WeatherBot(object):
    def __init__(self, token: str) -> None:
        self._token = token
        self._bot = telepot.Bot(self._token)
        self._cmd_handlers = collections.defaultdict(
            lambda: self.unknown_command_handler,
            {"/help": self.help_handler,
             "/set_celsius": self.celsius_handler,
             "/set_fahrenheit": self.fahrenheit_handler})
        self._chat_meta = collections.defaultdict(
            lambda: DEFAULT_META)

    def _send_temp_plot(self, chat_id: int,
                        weather: google.Weather) -> None:
        """Plots temperature plot and sends it to the chat."""
        is_c = self._chat_meta[chat_id].temp_degree == TemperatureDegree.Celsius
        temperatures = (weather.plot_c_temperatures
                        if is_c else weather.plot_f_temperatures)
        with utils.temp_folder(TEMP_DIRECTORY_NAME):
            ind = list(range(len(temperatures)))
            plt.bar(ind, temperatures, width=0.35)
            plt.ylabel('Температура')
            plt.xlabel('День недели')
            plt.xticks(ind, weather.plot_weekdays)
            plt.savefig(TEMP_PLOT_FILENAME, format='png', dpi=150)
            plt.clf()
            with open(TEMP_PLOT_FILENAME, "rb") as f:
                self._bot.sendPhoto(chat_id, f)

    def _send_location_picture(self, chat_id: int,
                               weather: google.Weather) -> None:
        """Sends location picture (via using Google Images) to the passed chat.

        :param chat_id: int. The chat id to send the picture to.
        :param weather: Weather tuple. The object that contains the location
            to search.
        :return: None.
        """
        location_name = weather.locaction
        print(location_name)
        location_name = " ".join(utils.text_to_words(location_name)) + " фото"
        with utils.temp_folder(TEMP_DIRECTORY_NAME):
            google_image.get_picture_for(location_name,
                                         folder=TEMP_DIRECTORY_NAME)
            for filename in utils.all_files_in(TEMP_DIRECTORY_NAME):
                with open(filename, "rb") as f:
                    self._bot.sendPhoto(chat_id, f)

    def _insert_temperature(self, weather: google.Weather, chat_id: int) -> str:
        if self._chat_meta[chat_id].temp_degree == TemperatureDegree.Celsius:
            return str(weather.c_temperature) + " °C"
        return str(weather.f_temperature) + " °F"

    def _compose_weather_msg(self, weather: google.Weather, chat_id: int) -> str:
        """Converts Weather tuple to its string representation."""
        return "\n".join([
            "📍 Где: " + str(weather.locaction),
            "🕒 Когда: " + str(weather.time),
            "☝ Погодные условия: " + str(weather.weather_condition),
            "🌡 Температура: " + self._insert_temperature(weather, chat_id),
            "🌧 Вероятность осадков: " + str(weather.precipitation) + "%",
            "💦 Влажность: " + str(weather.humidity) + "%",
            "🌬 " + weather.wind
        ])

    def _msg_handler(self, msg: dict):
        """Processes input messages and responds to them."""
        content_type, chat_type, chat_id = telepot.glance(msg)
        print(content_type, chat_type, chat_id)

        if content_type != 'text':
            self._bot.sendMessage(chat_id, "You sent something weird.")

        if msg['text'].startswith("/"):
            self._cmd_handlers[msg['text'].split()[0]](chat_type, chat_id, msg)
            return

        self._bot.sendMessage(chat_id, "Готовим погоду для вас 🔎🔎🔎...")
        try:
            weather = google.get_weather(msg['text'])
            weather_message = self._compose_weather_msg(weather, chat_id)
            self._send_location_picture(chat_id, weather)
            self._bot.sendMessage(chat_id, weather_message)
            self._send_temp_plot(chat_id, weather)
        except Exception as e:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            traceback.print_exception(exc_type, exc_value, exc_traceback,
                                      limit=10, file=sys.stdout)
            self._bot.sendMessage(chat_id, "Погода не приготовилась 😔")

    def get_me(self) -> dict:
        """Returns bot metadata."""
        return self._bot.getMe()

    def run(self) -> None:
        """Starts bot."""
        telepot.loop.MessageLoop(self._bot, self._msg_handler).run_forever()
        # telepot.loop.MessageLoop(self._bot, self._msg_handler).run_as_thread()
        # time.sleep(40)

    def unknown_command_handler(self, chat_type, chat_id, msg):
        self._bot.sendMessage(chat_id, "Unknown command {}".format(msg))

    def help_handler(self, chat_type, chat_id, msg):
        self._bot.sendMessage(
            chat_id,
            "Just type any request to get the result"
            "/help - get help."
            "/set_celsius - set Celsius degree."
            "/set_fahrenheit - set Fahrenheit degree.")

    def celsius_handler(self, chat_type, chat_id, msg):
        self._chat_meta[chat_id] = self._chat_meta[chat_id]._replace(
            temp_degree=TemperatureDegree.Celsius)

    def fahrenheit_handler(self, chat_type, chat_id, msg):
        self._chat_meta[chat_id] = self._chat_meta[chat_id]._replace(
            temp_degree=TemperatureDegree.Fahrenheit)



