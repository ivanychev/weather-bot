import collections
import itertools
import re

from typing import List

import bs4
import html2text
import requests

import utils

HEADER = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) '
                        'AppleWebKit/537.36 (KHTML, like Gecko) '
                        'Chrome/67.0.3396.18 Safari/537.36'}

Weather = collections.namedtuple("Weather", ["locaction",
                                             "time",
                                             "weather_condition",
                                             "c_temperature",
                                             "f_temperature",
                                             "precipitation",
                                             "humidity",
                                             "wind",
                                             "plot_weekdays",
                                             "plot_c_temperatures",
                                             "plot_f_temperatures"])


def extract_percent(raw: str):
    results = re.findall(r'(\d+)%', raw)
    return int(results[0])


def parse_precipitation(raw_weather: List[str]):
    index = utils.first_index_with_substr(raw_weather, "Вероятность осадков")
    return extract_percent(raw_weather[index])


def parse_humidity(raw_weather: List[str]):
    index = utils.first_index_with_substr(raw_weather, "Влажность")
    return extract_percent(raw_weather[index])


def parse_wind(raw_weather: List[str]):
    index = utils.first_index_with_substr(raw_weather, "Ветер")
    return raw_weather[index]


def parse_c_temperature(raw_weather: List[str]):
    index_of_cf = utils.first_index_with_substr(raw_weather, "°C")
    c_before_f = (raw_weather[index_of_cf].index("C") <
                  raw_weather[index_of_cf].index("F"))
    if c_before_f:
        return int(raw_weather[index_of_cf - 1][:-2])
    else:
        return int(raw_weather[index_of_cf - 1][-2:])

def parse_f_temperature(raw_weather: List[str]):
    index_of_cf = utils.first_index_with_substr(raw_weather, "°C")
    c_before_f = (raw_weather[index_of_cf].index("C") <
                  raw_weather[index_of_cf].index("F"))
    if c_before_f:
        return int(raw_weather[index_of_cf - 1][-2:])
    else:
        return int(raw_weather[index_of_cf - 1][:-2])

def parse_plot_weekdays(raw_weather: List[str]) -> List[str]:
    plot_indices = utils.indexes_with_substr(raw_weather, "onebox/weather")[1:]
    days_indices = [x - 1 - (len(raw_weather[x - 1]) != 2)
                    for x in plot_indices]

    return [raw_weather[i] for i in days_indices]

def parse_plot_c_temperatures(raw_weather: List[str]) -> List[int]:
    index_of_cf = utils.first_index_with_substr(raw_weather, "°C")
    c_before_f = (raw_weather[index_of_cf].index("C") <
                  raw_weather[index_of_cf].index("F"))
    plot_indices = utils.indexes_with_substr(raw_weather, "onebox/weather")[1:]
    temp_indices = (x + 1 for x in plot_indices)
    raw_temps = (raw_weather[i].split()[0] for i in temp_indices)
    return [int(temp[:-2] if c_before_f else temp[-2:]) for temp in raw_temps]

def parse_plot_f_temperatures(raw_weather: List[str]) -> List[int]:
    index_of_cf = utils.first_index_with_substr(raw_weather, "°C")
    c_before_f = (raw_weather[index_of_cf].index("C") <
                  raw_weather[index_of_cf].index("F"))
    plot_indices = utils.indexes_with_substr(raw_weather, "onebox/weather")[1:]
    temp_indices = (x + 1 for x in plot_indices)
    raw_temps = (raw_weather[i].split()[0] for i in temp_indices)
    return [int(temp[-2:] if c_before_f else temp[:-2]) for temp in raw_temps]


def raw_weather_resp_parser(raw_weather: List[str]):
    return Weather(raw_weather[0],
                   raw_weather[1],
                   raw_weather[2],
                   parse_c_temperature(raw_weather),
                   parse_f_temperature(raw_weather),
                   parse_precipitation(raw_weather),
                   parse_humidity(raw_weather),
                   parse_wind(raw_weather),
                   parse_plot_weekdays(raw_weather),
                   parse_plot_c_temperatures(raw_weather),
                   parse_plot_f_temperatures(raw_weather))


def get_weather(request: str):
    request = [s.lower() for s in request.split()]
    if "погода" not in request:
        request = ["погода"] + request
    url = "https://www.google.ru/search?q={}&hl=ru".format(
        "+".join(request)
    )
    response = requests.get(url, headers=HEADER)
    bs = bs4.BeautifulSoup(response.text, "html5lib")
    card = bs.findAll("div", {"class": "card-section"})[0]
    h = html2text.HTML2Text()
    h.ignore_links = True
    plain_text_tile = h.handle(str(card))
    raw_weather = [s for s in plain_text_tile.split("\n") if s]
    # with open("current_raw_response.txt", "w") as f:
    #     print(plain_text_tile)

    # print(raw_weather)
    return raw_weather_resp_parser(raw_weather)
