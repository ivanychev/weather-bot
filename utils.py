import contextlib
import os
import re
import shutil

from typing import List


WORD_REGEX = r'(?u)\w+'

def all_files_in(directory: str):
    """Returns paths of all files existing in current directory."""
    for subdir, dirs, files in os.walk(directory):
        yield from (os.path.join(subdir, file) for file in files)


def text_to_words(text: str):
    """Splits text and returns generator of words in it."""
    for match in re.finditer(WORD_REGEX, text, flags=re.I):
        word = match.group()
        yield word.lower()


def first_true(iterable, default=False, pred=None):
    return next(filter(pred, iterable), default)


def indexes_with_substr(words: List[str], substr: str) -> List[int]:
    return list(idx
                for idx, word in enumerate(words)
                if substr in word)


def first_index_with_substr(words: List[str], substr: str) -> int:
    index, value = first_true(enumerate(words),
                              pred=lambda is_pair: substr in is_pair[1])
    return index

@contextlib.contextmanager
def temp_folder(folder_name: str):
    if not os.path.exists(folder_name):
        os.makedirs(folder_name)
    yield
    shutil.rmtree(folder_name, ignore_errors=True)
